import fs from "fs";

import { parseXMLData } from "./utils/parser.js";

const inputData = fs.createReadStream("src/data/inputData.xml");
const outputData = fs.createWriteStream("src/data/outputData.json");

parseXMLData(inputData, outputData);
