import flow from "xml-flow";

import { save } from "../db/database-stub.js";

export const parseXMLData = (inputData, outputData) => {
  console.info(
    `XML document parsing started at: ${new Date().toLocaleTimeString()}\n`,
  );

  const xmlStream = flow(inputData, { strict: true, preserveMarkup: -1 });
  let messageslist = [];
  let messageObject = {};

  xmlStream.on("error", (error) => {
    console.error(
      `An error occured during XML document parsing: ${error?.message}`,
    );
  });

  xmlStream.on("convertToJson", () => {
    /* Here we could implement some kind of batch or iterative insert of messages into DB with one db.save() call
		In requirements said that no need to implement it, so just writing the result in JSON file. */
    outputData.write(JSON.stringify(messageslist), (error) => {
      error
        ? xmlStream.emit("error", error)
        : console.info(
            `XML document parsing succesfuly completed at: ${new Date().toLocaleTimeString()}`,
          );
    });
  });

  xmlStream.on("tag:Message", (xmlNode) => {
    if (xmlNode?.From && xmlNode?.Message) {
      messageObject = {
        From: xmlNode.From,
        Message:
          typeof xmlNode.Message === "string"
            ? xmlNode.Message
            : xmlNode.Message?.$text.join(" "),
      };
      // db.save() emulation just passing message object in JSON format.
      save(JSON.stringify(messageObject));

      // This step is optional, just for collecting all XML data in JSON file for further usage.
      messageslist.push(messageObject);
    }
  });

  xmlStream.on("end", () => {
    // Optional step if we want to save result of parsing into separate JSON file.
    xmlStream.emit("convertToJson");
  });
};
